import requests as requests
import Settings as Settings
import pymysql as pymysql
import pandas as pd
auth = requests.auth.HTTPBasicAuth(Settings.username, Settings.pwd)

def getMySqlConnection():
    return pymysql.connections.Connection(Settings.db_host, Settings.db_user,
                                          Settings.db_pwd, Settings.db_name,
                                          Settings.db_port)

def getDbData(query, connection):
    curs = connection.cursor()
    return curs.execute(query)

def sendRequest(url, data):
    requests.put(url, data=data, auth=auth)

def bulkReques(curs, url):
    i = 0
    for row, i in curs:
        data = {
        'club': row['name'],
        'city': row['city_name'],
        'description': row['description']
        }
        reqUrl=url+str(i)
        sendRequest(reqUrl,data)
        i+=1


url = 'localhost:9200/groover/events'
conn = getMySqlConnection()
curs = getDbData(Settings.db_club_query, conn)
bulkReques(curs, url)



            
